FROM jinhongc/oceanbase-ce:1


ENV VERSION=4.3.0.1-100000242024032211
RUN yum install -y obproxy-ce prometheus grafana ocp-express oblogproxy-2.0.0-101000012023121819.el7
CMD export PATH=$PATH:/home/admin/obproxy-4.2.3.0/bin

COPY ./obdeploy/example/all-components-cluster-init.yml /all-components-cluster-init.yml
# Copy to running container: docker cp ./obdeploy/example/all-components-cluster-init.yml cc2955d3bcdbe267fa920141dfd50f5c41f7366641be09f883f76f3aca9cb3dc:/all-components-cluster-init.yml

# `obd demo` works?

# need configure keyless ssh:
# sudo yum –y install openssh-server openssh-clients
# 

# mkdir -p /run/sshd && chmod 755 /run/sshd
# ssh-keygen -A
# /usr/sbin/sshd
# usermod --password 58iamababy root
# ssh localhost 

# 1. yes '' | ssh-keygen -N ''
# Press enter for each line 
# 2. cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys
# 3. chmod og-wx ~/.ssh/authorized_keys 

# need add known host keys



# -- look into obd (obdeployer) code to debug
# `obd` command: https://github.com/oceanbase/obd-doc/blob/V1.6.1/en-US/300.user-guide/300.obd-command/000.obd-demo.md
# source code: https://github.com/oceanbase/obdeploy/blob/master/core.py
# entry point: https://github.com/oceanbase/obdeploy/blob/master/_cmd.py
# " obd.deploy_cluster(name) and obd.start_cluster(name)"
# run obd command: https://en.oceanbase.com/docs/community-obd-en-10000000001165755

# debug process: https://github.com/oceanbase/oceanbase/issues/87

# debug to server binary process level, track their logs, etc

CMD obd cluster deploy ob_cluster_name_cluster_a -c /all-components-cluster-init.yml # home_path and data_dir are in this file.
CMD obd cluster start ob_cluster_name_cluster_a

# startup debug code path:
# obdeploy code --build--> obd binary --> invoke plugins --> inoke binary program
# example binary program: `clusters_cmd[server] = 'cd %s; %s/bin/observer %s' % (home_path, home_path, ' '.join(cmd))`
#https://github.com/oceanbase/obdeploy/blob/16f14f5911625380bfdf0c648821a2ebb988d08c/core.py#L1369
#https://github.com/oceanbase/obdeploy/blob/16f14f5911625380bfdf0c648821a2ebb988d08c/core.py#L335
#https://github.com/oceanbase/obdeploy/blob/16f14f5911625380bfdf0c648821a2ebb988d08c/plugins/ob-configserver/1.0.0/start.py
#https://share.zight.com/o0uLbQEb
#https://share.zight.com/geu7jr4Q
#https://share.zight.com/OAuZ0LQz
#https://share.zight.com/d5uXLe10
#https://share.zight.com/8LuN2wob
#https://share.zight.com/9Zu9vNQR
#https://share.zight.com/rRuDpLbQ



# can configure cluster but cannot start cluster.
# error message: "Some servers have been started.", to retry?

# binary path is in /root/ob/bin/observer


# ~/.oceanbase-all-in-one/obd/usr/bin/obd cluster deploy ob_cluster_name_cluster_a -c /all-components-cluster-init.yml

# View the list of clusters managed by OBD.
# obd cluster list

# View the status of the obtest cluster.
# obd cluster display obtest

# Initialize DB


# run the server on background
# start the container with -dit to keep it from exiting.
# docker run -dit ubuntu

# TODO: `Dockerfile` need use the all-in-one package instead.
# TODO: try install ssh to `Dockerfile` image for cluster deployment, need keyless ssh
# TODO: 1) home_path and data_dir map to local volume; 2) observer port mapping


# Verification, connecting to the db:
# Connect without creating user db: obclient -h127.0.0.1 -P2883 -uroot -prootPWD123 -Doceanbase